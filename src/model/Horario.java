package model;

public class Horario {
	private Aluno aluno;
	private boolean seg;
	private boolean ter;
	private boolean qua;
	private boolean qui;
	private boolean sex;
	
	public boolean isSeg() {
		return seg;
	}
	
	public void setSeg(boolean seg) {
		this.seg = seg;
	}
	
	public boolean isTer() {
		return ter;
	}
	
	public void setTer(boolean ter) {
		this.ter = ter;
	}
	
	public boolean isQua() {
		return qua;
	}
	
	public void setQua(boolean qua) {
		this.qua = qua;
	}
	
	public boolean isQui() {
		return qui;
	}
	
	public void setQui(boolean qui) {
		this.qui = qui;
	}
	
	public boolean isSex() {
		return sex;
	}
	
	public void setSex(boolean sex) {
		this.sex = sex;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
}
