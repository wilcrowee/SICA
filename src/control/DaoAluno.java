package control;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;

import control.ConectionFactory;
import model.Aluno;

public class DaoAluno {
	private final String url = "jdbc:postgresql://127.0.0.1:5432/sica_dev";
	private final String nome = "postgres";
	private final String senha = "senha";
	private Connection con;
	private Statement comando;
	
	private void conectar(){
		try{
			con = ConectionFactory.conexao(url, nome, senha, ConectionFactory.POSTGRESQL);
			comando = con.createStatement();
			System.out.println("Conectado");
		} catch (ClassNotFoundException e) {
	        imprimeErro("Erro ao carregar o driver", e.getMessage());  
		} catch (SQLException e) {
	        imprimeErro("Erro ao conectar", e.getMessage());  
		}
	}
	
	private void fechar() {  
		try {  
			comando.close();  
			con.close();  
			System.out.println("Conexão Fechada");  
		} catch (SQLException e) {  
			imprimeErro("Erro ao fechar conexão", e.getMessage());  
		}  
	}  
	
	private void imprimeErro(String msg, String msgErro) {  
		JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);  
		System.err.println(msg);  
		System.out.println(msgErro);  
		System.exit(0);  
	}
	
	public Vector<Aluno> buscar(String prontuario){
		conectar();
		Vector<Aluno> resultados = new Vector<Aluno>();
		ResultSet rs;
		
		try{
			rs =  comando.executeQuery("Select * from aluno_aluno where prontuario LIKE \'"+prontuario+"\';");
			
			while (rs.next()){
				Aluno temp = new Aluno();
				temp.setProntuario(rs.getString("prontuario"));
				temp.setNome(rs.getString("nome"));
				temp.setSobrenome(rs.getString("sobrenome"));
				temp.setCurso(Integer.parseInt(rs.getString("curso")));
				temp.setId(Integer.parseInt(rs.getString("id")));
				temp.setSenha(rs.getString("senha"));
				resultados.add(temp);
			}
			
			fechar();
			return resultados;
		} catch (SQLException e){
			fechar();
			imprimeErro("Erro ao buscar aluno", e.getMessage());
			return null;
		}
	}
}
