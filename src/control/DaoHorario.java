package control;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JOptionPane;

import model.Aluno;
import model.Horario;

public class DaoHorario {
	private final String url = "jdbc:postgresql://127.0.0.1:5432/sica_dev";
	private final String nome = "postgres";
	private final String senha = "senha";
	private Connection con;
	private Statement comando;
	
	private void conectar(){
		try{
			con = ConectionFactory.conexao(url, nome, senha, ConectionFactory.POSTGRESQL);
			comando = con.createStatement();
			System.out.println("Conectado");
		} catch (ClassNotFoundException e) {
	        imprimeErro("Erro ao carregar o driver", e.getMessage());  
		} catch (SQLException e) {
	        imprimeErro("Erro ao conectar", e.getMessage());  
		}
	}
	
	private void fechar() {  
		try {  
			comando.close();  
			con.close();  
			System.out.println("Conexão Fechada");  
		} catch (SQLException e) {  
			imprimeErro("Erro ao fechar conexão", e.getMessage());  
		}  
	}  
	
	private void imprimeErro(String msg, String msgErro) {  
		JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);  
		System.err.println(msg);  
		System.out.println(msgErro);  
		System.exit(0);  
	}
	
	public Vector<Horario> buscar (Aluno aluno){
		conectar();
		Vector<Horario> resultados = new Vector<Horario>();
		ResultSet rs;
		
		try{
			rs =  comando.executeQuery("Select * from horario_horario where aluno_id ="+aluno.getId()+";");
			
			while (rs.next()){
				Horario temp = new Horario();
				temp.setAluno(aluno);
				temp.setSeg(rs.getBoolean(2));
				temp.setTer(rs.getBoolean(3));
				temp.setQua(rs.getBoolean(4));
				temp.setQui(rs.getBoolean(5));
				temp.setSex(rs.getBoolean(6));
				resultados.add(temp);
			}
			
			fechar();
			return resultados;
		} catch (SQLException e){
			fechar();
			imprimeErro("Erro ao buscar horario", e.getMessage());
			return null;
		}
	}
}
