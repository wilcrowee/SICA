package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class DaoRefeicao {
	/private final String url = "jdbc:postgresql://127.0.0.1:5432/sica_dev"; 
	private final String nome = "postgres";
	private final String senha = "senha";
	private Connection con;
	private Statement comando;
	
	private void conectar(){
		try{
			con = ConectionFactory.conexao(url, nome, senha, ConectionFactory.POSTGRESQL);
			comando = con.createStatement();
			System.out.println("Conectado");
		} catch (ClassNotFoundException e) {
	        imprimeErro("Erro ao carregar o driver", e.getMessage());  
		} catch (SQLException e) {
	        imprimeErro("Erro ao conectar", e.getMessage());  
		}
	}
	
	private void fechar() {  
		try {  
			comando.close();  
			con.close();  
			System.out.println("Conexão Fechada");  
		} catch (SQLException e) {  
			imprimeErro("Erro ao fechar conexão", e.getMessage());  
		}  
	}  
	
	private void imprimeErro(String msg, String msgErro) {  
		JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);  
		System.err.println(msg);  
		System.out.println(msgErro);  
		System.exit(0);  
	}
	
	public boolean inserirRefeicao(int id){
		int dia, mes, ano;
		Calendar  c =  Calendar.getInstance();
		ResultSet rs;
		
		dia = c.get(Calendar.DAY_OF_MONTH);
		mes = c.get(Calendar.MONTH)+1;
		ano = c.get(Calendar.YEAR);
				
		conectar();
		
		try{
			rs =  comando.executeQuery("Select * from relatorio_refeicao where aluno_id = "+id+" and data = to_date('"+dia+"-"+mes+"-"+ano+"','dd-mm-yyyy');");
			
			if(!rs.next()){
				PreparedStatement stmt = con.prepareStatement("insert into relatorio_refeicao(data,aluno_id) values (?,?);");
				stmt.setDate(1, new java.sql.Date(c.getTimeInMillis()));
				stmt.setInt(2, id);
				stmt.execute();
				fechar();
				return true;
			}
			else{
				System.out.println("Ja cadastrado");
				fechar();
				return false;
			}
			
		} catch (SQLException e){
			fechar();
			imprimeErro("Erro ao buscar ticket", e.getMessage());
			return false;
		}			
	}
}
