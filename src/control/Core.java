package control;

import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import view.Principal;

public class Core {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame frame = new JFrame("SICA - Refeição");
			        Principal window = new Principal();
			        ImageIcon img = new ImageIcon(getClass().getClassLoader().getResource("img/Food-Dome.png"));
			        
			        frame.setIconImage(img.getImage());
			        frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
			        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			        frame.getContentPane().add(window);
			        frame.pack();
			        frame.setIconImage(img.getImage());
			        frame.setLocationRelativeTo(null);
			        frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
