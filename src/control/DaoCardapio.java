package control;

import model.Cardapio;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class DaoCardapio {
	private final String url = "jdbc:postgresql://127.0.0.1:5432/sica_dev";
	private final String nome = "postgres";
	private final String senha = "senha";
	private Connection con;
	private Statement comando;
	
	private void conectar(){
		try{
			con = ConectionFactory.conexao(url, nome, senha, ConectionFactory.POSTGRESQL);
			comando = con.createStatement();
			System.out.println("Conectado");
		} catch (ClassNotFoundException e) {
	        imprimeErro("Erro ao carregar o driver", e.getMessage());  
		} catch (SQLException e) {
	        imprimeErro("Erro ao conectar", e.getMessage());  
		}
	}
	
	private void fechar() {  
		try {  
			comando.close();  
			con.close();  
			System.out.println("Conexão Fechada");  
		} catch (SQLException e) {  
			imprimeErro("Erro ao fechar conexão", e.getMessage());  
		}  
	}  
	
	private void imprimeErro(String msg, String msgErro) {  
		JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);  
		System.err.println(msg);  
		System.out.println(msgErro);  
		System.exit(0);  
	}
	
	public Cardapio buscarCardapio(){
		Cardapio card = new Cardapio();
		Calendar c = Calendar.getInstance();
		String data;
		ResultSet rs;
		
		conectar();
		
		try{
			data = c.get(Calendar.DAY_OF_MONTH)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
			rs =  comando.executeQuery("Select * from core_cardapio where data = to_date('"+data+"','dd/mm/yyyy');");		
			while (rs.next()){
				card.setPrincipal(" "+rs.getString(2));
				card.setSalada(" "+rs.getString(3));
				card.setSobremesa(" "+rs.getString(4));
				card.setSuco(" "+rs.getString(5));
			}
			
			fechar();
			return card;
		} catch (SQLException e){
			fechar();
			imprimeErro("Erro ao buscar cardapio", e.getMessage());
			card.setPrincipal("Erro");
			card.setSalada("Erro");
			card.setSobremesa("Erro");
			card.setSuco("Erro");
			return card;
		}
	}
	
}
