package view;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.Timer;

import org.apache.commons.lang3.StringUtils;

import control.*;
import model.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class Principal extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;
	public Principal() {
		
		DaoCardapio daoCardapio = new DaoCardapio();
		Cardapio card = daoCardapio.buscarCardapio();
		
		initComponents();
		       
		if(card.getPrincipal() == null) {
			lblTxPrato.setText("<html><body><div style='padding: 10px; font-size: 14px;'><p style='padding: 5px;'>"+"Nenhum Registro para o Dia"+"</p></div></body></html>");
		}else {
			lblTxPrato.setText("<html><body><div style='padding: 10px; font-size: 14px;'><p style='padding: 5px;'>"+card.getPrincipal().replace(";", "</p><p style='padding: 5px;'>")+"</p></div></body></html>");
		}
		if(card.getSalada() == null) {
			lblTxSalada.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+"Nenhum Registro para o Dia"+"</div></body></html>");	
		}else {
			lblTxSalada.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+card.getSalada()+"</div></body></html>");
		}
		if(card.getSobremesa() == null) {
			lblTxSobremesa.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+"Nenhum Registro para o Dia"+"</div></body></html>");
		}else {
			lblTxSobremesa.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+card.getSobremesa()+"</div></body></html>");
		}
		if(card.getSobremesa() == null) {
			lblTxSuco.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+"Nenhum Registro para o Dia"+"</div></body></html>");
		}else {
			lblTxSuco.setText("<html><body><div style='padding: 10px; font-size: 14px;'>"+card.getSuco()+"</div></body></html>");
		}
	}

    private void initComponents() {

        lblProntuario = new javax.swing.JLabel();
        tfProntuario = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblSobrenome = new javax.swing.JLabel();
        tfNome = new javax.swing.JTextField();
        tfSobrenome = new javax.swing.JTextField();
        lblCurso = new javax.swing.JLabel();
        tfCurso = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        lblStatus = new javax.swing.JLabel();
        lblStatus.setForeground(Color.GREEN);
        jPanel3 = new javax.swing.JPanel();
        lblPrato = new javax.swing.JLabel();
        lblTxPrato = new javax.swing.JLabel();
        lblSalada = new javax.swing.JLabel();
        lblTxSalada = new javax.swing.JLabel();
        lblSobremesa = new javax.swing.JLabel();
        lblTxSobremesa = new javax.swing.JLabel();
        lblSuco = new javax.swing.JLabel();
        lblTxSuco = new javax.swing.JLabel();

        lblProntuario.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblProntuario.setText("Prontuário:");

        tfProntuario.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfProntuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					tfProntuarioActionPerformed(evt);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });


        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dados do Aluno:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 14))); // NOI18N

        lblNome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblNome.setText("Nome:");

        lblSobrenome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSobrenome.setText("Sobrenome:");

        tfNome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfNome.setEnabled(false);

        tfSobrenome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfSobrenome.setEnabled(false);

        lblCurso.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblCurso.setText("Curso:");

        tfCurso.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfCurso.setEnabled(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 14))); // NOI18N

        lblStatus.setFont(new java.awt.Font("Noto Sans", 1, 42)); // NOI18N
        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblNome)
                        .addGap(49, 49, 49)
                        .addComponent(tfNome, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCurso)
                        .addGap(49, 49, 49)
                        .addComponent(tfCurso))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblSobrenome)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfSobrenome)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSobrenome)
                    .addComponent(tfSobrenome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCurso))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cardapio do dia:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N

        lblPrato.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblPrato.setText("Prato Principal:");

        lblTxPrato.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSalada.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSalada.setText("Salada:");

        lblTxSalada.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSobremesa.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSobremesa.setText("Sobremesa:");

        lblTxSobremesa.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSuco.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSuco.setText("Suco:");

        lblTxSuco.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTxPrato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSalada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSobremesa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSuco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPrato)
                            .addComponent(lblSalada)
                            .addComponent(lblSobremesa)
                            .addComponent(lblSuco))
                        .addGap(0, 453, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPrato)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxPrato, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSalada)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSalada, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSobremesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSobremesa, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSuco)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSuco, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProntuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfProntuario))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblProntuario)
                            .addComponent(tfProntuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfProntuarioActionPerformed(java.awt.event.ActionEvent evt) throws InterruptedException, ParseException{//GEN-FIRST:event_tfProntuarioActionPerformed
		DaoAluno daoAluno = new DaoAluno();
		DaoTicket daoTicket = new DaoTicket();
		DaoRefeicao daoRefeicao = new DaoRefeicao();
				
		Vector<Aluno> resultado;
		Calendar c = Calendar.getInstance();
		
		SimpleDateFormat formatador = new SimpleDateFormat("HH:mm");
		Date dMin = formatador.parse("06:40");
		Date dMax = formatador.parse("09:10");
		Date dAgora = formatador.parse(c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE));
		String msg;		
		
		if(dAgora.getTime() > dMin.getTime() && dAgora.getTime() < dMax.getTime()){
			lblStatus.setForeground(Color.red);
			lblStatus.setText("Terminal Invalido");
			msg = "Este terminal não gera ticket.";
			JOptionPane MsgBox = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE, JOptionPane.CLOSED_OPTION);
			limpaCampos();
			messageJOP(msg, MsgBox);
		}else{
			dMin = formatador.parse("12:20");
			dMax = formatador.parse("13:30");
				
			dAgora = formatador.parse(c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE));	
			
			if(dAgora.getTime() > dMin.getTime() && dAgora.getTime() < dMax.getTime()){

				String prontuario = tfProntuario.getText().toUpperCase();
				
				if(!StringUtils.isNumeric(prontuario.substring(0,2)))
					prontuario = prontuario.substring(2, prontuario.length());
					
				resultado = daoAluno.buscar(prontuario);
				
				if (resultado.isEmpty()){
					lblStatus.setForeground(Color.red);
					lblStatus.setText("Não Cadastrado");
					limpaCampos();
					msg = "O aluno não esta cadastrado no sistema";
					JOptionPane MsgBox = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE, JOptionPane.CLOSED_OPTION);
					messageJOP(msg, MsgBox);	
				}else{				
					if(daoTicket.buscarTicket(resultado.get(0).getId())){
						int tipo = 0;
						
						if(validaSenha(resultado.get(0))){
							if(daoRefeicao.inserirRefeicao(resultado.get(0).getId())){
								lblStatus.setForeground(Color.green);
								lblStatus.setText("Aluno liberado");
								preencheCampos(resultado);
								msg = "Aluno liberado para almoço";
								tipo = JOptionPane.INFORMATION_MESSAGE;
							}else{
								lblStatus.setForeground(Color.red);
								lblStatus.setText("Já Contabilizada");
								preencheCampos(resultado);
								msg = "Aluno já almoçou no dia";
							}
							
							limpaCampos();
							JOptionPane MsgBox = new JOptionPane(msg, tipo, JOptionPane.CLOSED_OPTION);
							messageJOP(msg, MsgBox);
						}else {
							lblStatus.setForeground(Color.red);
							lblStatus.setText("Senha Errada");
							preencheCampos(resultado);
							msg = "Senha Errada";
							limpaCampos();
							JOptionPane MsgBox = new JOptionPane(msg, tipo, JOptionPane.CLOSED_OPTION);
							messageJOP(msg, MsgBox);
						}
					}else{
						lblStatus.setForeground(Color.red);
						lblStatus.setText("Aluno Bloqueado");
						preencheCampos(resultado);
						limpaCampos();
						msg = "Aluno não pode almoçar";
						JOptionPane MsgBox = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE, JOptionPane.CLOSED_OPTION);
						messageJOP(msg, MsgBox);	
					}
				}
			}else{
				lblStatus.setForeground(Color.red);
				lblStatus.setText("Fora do horário");
				msg = "Fora do horário de serviço";
				JOptionPane MsgBox = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE, JOptionPane.CLOSED_OPTION);
				limpaCampos();
				messageJOP(msg, MsgBox);
				
			}
		}
    }

    private void preencheCampos (Vector<Aluno> resultado){
    	tfNome.setText(resultado.get(0).getNome());
		tfSobrenome.setText(resultado.get(0).getSobrenome());
		switch (resultado.get(0).getCurso()){
			case 1:
				tfCurso.setText("Técnico Integrado em Informática");
				break;
			case 2:
				tfCurso.setText("Técnico Integrado em Mecatrônica");
				break;
		}
    }
    
    private void limpaCampos () throws InterruptedException{
    	Timer timer = new Timer(2500, new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				tfProntuario.setText("");
		    	tfNome.setText("");
		    	tfSobrenome.setText("");
		    	tfCurso.setText("");
		    	lblStatus.setText("");
			}
		});
    	
    	timer.setRepeats(false);
    	timer.start();   	
    }

    private void messageJOP(String msg, JOptionPane MsgBox){
    	JDialog dlg = MsgBox.createDialog("Aviso");
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dlg.addComponentListener(new ComponentAdapter() {
          @Override
          // =====================
          public void componentShown(ComponentEvent e) {
          // =====================
            super.componentShown(e);
            Timer t; t = new Timer(2000, new ActionListener() {
              @Override
              // =====================
              public void actionPerformed(ActionEvent e) {
              // =====================
                dlg.setVisible(false);
              }
            });
            t.setRepeats(false);
            t.start();
          }
        });
        dlg.setVisible(true);
        Object n = MsgBox.getValue();
        System.out.println(n);
        System.out.println("Finished");
        dlg.dispose();
    }
    
    private boolean validaSenha(Aluno resultado) {
    	if(!resultado.getSenha().equals("") && !resultado.getSenha().equals(" ")){
    		JLabel label = new JLabel("Digite a senha:");
			JPasswordField jpf = new JPasswordField();
			JOptionPane optionPane = new JOptionPane(new Object[]{label, jpf}, JOptionPane.OK_CANCEL_OPTION) {
				 @Override
				 public void selectInitialValue()
				 {
					 jpf.requestFocusInWindow();
				 }
			};
			
			optionPane.createDialog(null, "Senha").setVisible(true);
			String senha = new String(jpf.getPassword());
			
			if(!resultado.getSenha().equals(senha)){
				return false;
			}
    	}
    	return true;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblCurso;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPrato;
    private javax.swing.JLabel lblProntuario;
    private javax.swing.JLabel lblSalada;
    private javax.swing.JLabel lblSobremesa;
    private javax.swing.JLabel lblSobrenome;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblSuco;
    private javax.swing.JLabel lblTxPrato;
    private javax.swing.JLabel lblTxSalada;
    private javax.swing.JLabel lblTxSobremesa;
    private javax.swing.JLabel lblTxSuco;
    private javax.swing.JTextField tfCurso;
    private javax.swing.JTextField tfNome;
    private javax.swing.JTextField tfProntuario;
    private javax.swing.JTextField tfSobrenome;
    // End of variables declaration//GEN-END:variables
}
